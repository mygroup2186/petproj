# 
FROM python:3.8

# 
WORKDIR /code

# 
COPY . .

# 
RUN pip3 install --no-cache-dir --upgrade -r /code/requirements.txt

# 
CMD ["uvicorn", "main:app", "--port", "80"]
